I test the following site:  https://funimation1.herokuapp.com/

Here are my API service tests, made in Postman. 
Public link to the collection: https://documenter.getpostman.com/view/8263656/SW7gTjro
To run the collection in terminal using Newman you need to access from terminal the folder which include the json file for THE ENVURONMENT and there to write: 

newman run "https://www.getpostman.com/collections/bc849fb39f549e399f79") --environment "baseUrl_environment.json"

The test collection will run in the terminal.

********************************************************************************

With a registration in Postman (Go to getpostman.com and sign up), you can see and explore the tests collection in the following link:
https://documenter.getpostman.com/view/8263656/SW7gTjro?version=latest

The next way to see and explore the API collection is to import these two json files in your Postman:
The colletion is in Funanimation_collection.postman_collection.json and
the environment which the collection need to run is in baseUrl_environment.json. 

********************************************************************************

FUNIMATION MOCK SERVER

I made a Mock Server for the same website: https://funimation1.herokuapp.com/
Each query has several examples for the different response from the server. 
Each query is tested with several tests, tested are the headers, the status, the jason content in the responses.
Here is the documentation: https://documenter.getpostman.com/view/8263656/SW7gV5wS?version=latest

********************************************************************************
### CI/CD in Gitlab using Newman
In this project i demonstrate how we can work using CI/CD Gitlab functionallity.

For this i use the collection: Testing.postman_collection.json  and gitlab-ci.yml file.
Next we go in CI/CD -> Pipeline and click Run Pipline. 
The result we can find in CI/CD -> Jobs and can download as html file: report.html

Happy testing!